//package com.reger.datasource.aspect;
//
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.springframework.core.Ordered;
//import org.springframework.core.annotation.Order;
//
///**
// * 注解的方法，调用时会切换到指定的数据源
// *@author leige
// */
//@Aspect
//@Order(Ordered.HIGHEST_PRECEDENCE)
//public class DataSourceAspect {
//	
//	@Around(value = "@annotation(com.reger.datasource.annotation.DataSourceChange)", argNames = "point")
//	public Object doAround(final ProceedingJoinPoint point) throws Throwable {
//		return point.proceed();
//	}
//
//}
